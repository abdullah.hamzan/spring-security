//package com.springsecurity.springsecurity.config;
//
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.provisioning.InMemoryUserDetailsManager;
//
//@Configuration
//public class UserConfig {
//
//    @Bean
//    public UserDetailsService usersupdate(){
//
//        UserDetails admin= User.builder()
//                .username("admin")
//                .password("{noop}password")
//                .roles("ADMIN")
//                .build();
//
//        UserDetails user= User.builder()
//                .username("user")
//                .password("{bycrypt}$2a$12$uraQOI5Z.04AUAyyy25k7udeWzll1KYp2ehssH1hpXVgF8xQjEHDu")
//                .roles("USER")
//                .build();
//
//
//        return new InMemoryUserDetailsManager(admin,user);
//
//    }
//}
