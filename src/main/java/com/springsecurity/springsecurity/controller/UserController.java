package com.springsecurity.springsecurity.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class UserController {

    @GetMapping("/abdul")
    public ResponseEntity<?> sayHello(){
        return ResponseEntity.ok("Hello");
    }


}
