package com.springsecurity.springsecurity.payload;


import lombok.Data;

@Data
public class ResponseToken {
    private String token;
}
