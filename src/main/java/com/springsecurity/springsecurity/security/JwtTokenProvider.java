package com.springsecurity.springsecurity.security;


import com.springsecurity.springsecurity.model.User;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;

import io.jsonwebtoken.security.SignatureException;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@Component
public class JwtTokenProvider {
    private final Key key= Keys.secretKeyFor(SignatureAlgorithm.HS256);

    private Long expiration=1000L*60*60; // ms:m:h

    public String generateToken(Authentication authentication){

         final User user= (User) authentication.getPrincipal();
        Date now= new Date(System.currentTimeMillis());

        Date expiredDate= new Date(now.getTime()+expiration);
        Map<String,Object> claims= new HashMap<>();
        claims.put("username",user.getUsername());

        return Jwts.builder().setId(user.getId().toString())
                .setSubject(user.getUsername())
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(expiredDate)
                .signWith(key)
                .compact();



    }

    public boolean validateToken(String token){
        try{
            Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
            return true;

        }catch (SignatureException e){
            log.error("Invalid Jwt Signature: {}",e.getMessage());
        }catch (MalformedJwtException e){
            log.error("Invalid Jwt Token: {}",e.getMessage());
        }catch (ExpiredJwtException e){
            log.error("Expire Jwt Token: {}",e.getMessage());
        }catch (UnsupportedJwtException e){
            log.error("Unsupport Jwt Token: {}",e.getMessage());
        }catch (IllegalArgumentException e){
            log.error("Jwt claimstring is empaty: {}",e.getMessage());
        }

        return false;

    }
    public String getUsername(String token){
        Claims claims=Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJwt(token).getBody();
        return claims.get("username").toString();

    }

}
