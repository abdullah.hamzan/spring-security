package com.springsecurity.springsecurity.service.impl;


import com.springsecurity.springsecurity.model.User;
import com.springsecurity.springsecurity.repository.UserRepository;
import com.springsecurity.springsecurity.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user=userRepository.getDistinctTopByusername(username);

        if (user==null){
            throw new UsernameNotFoundException("user not found");
        }
        return null;
    }
}
