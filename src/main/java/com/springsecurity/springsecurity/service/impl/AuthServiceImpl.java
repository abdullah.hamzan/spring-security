package com.springsecurity.springsecurity.service.impl;
import com.springsecurity.springsecurity.model.User;
import com.springsecurity.springsecurity.payload.ResponseToken;
import com.springsecurity.springsecurity.payload.UsernamePassword;
import com.springsecurity.springsecurity.repository.UserRepository;
import com.springsecurity.springsecurity.security.JwtTokenProvider;
import com.springsecurity.springsecurity.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Log4j2
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;

    @Override
    public User register(UsernamePassword req) {
        User user= new User();
        user.setUsername(req.getUsername());
        user.setPassword(passwordEncoder.encode(req.getPassword()));
        return userRepository.save(user);

    }

    @Override
    public ResponseToken generateToken(UsernamePassword req) {

        try{

            Authentication authentication= authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            req.getUsername(),
                            req.getPassword()
                    )
            );


            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt= jwtTokenProvider.generateToken(authentication);
            ResponseToken responseToken= new ResponseToken();
            responseToken.setToken(jwt);
            return responseToken;


        }catch (BadCredentialsException e){
            log.error("Bad Credential",e);

            throw  new RuntimeException(e.getMessage(),e);

        }catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }


    }
}
