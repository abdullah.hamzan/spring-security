package com.springsecurity.springsecurity.service;


import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

}
