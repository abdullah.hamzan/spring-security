package com.springsecurity.springsecurity.service;


import com.springsecurity.springsecurity.model.User;
import com.springsecurity.springsecurity.payload.ResponseToken;
import com.springsecurity.springsecurity.payload.UsernamePassword;

public interface AuthService {
    User register(UsernamePassword req);
    ResponseToken generateToken(UsernamePassword req);

}
